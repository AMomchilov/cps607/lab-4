typealias long = UInt32
typealias int = UInt16


let ProcessorClock: long 	= 16_000_000 //15_990_784; // in Hz
let ClockDivider: long = 8;
let PWMFrequency: long = 100; // in Hz
let PWMPeriod: int = int(ProcessorClock / (PWMFrequency * ClockDivider));	// PWM Period in counter ticks

func toCounterTicks(microSeconds: int) -> int {
	let microsecondsPerSecond: long  = 1_000_000
	let clockTicksPerMicrosecond: long = ProcessorClock / microsecondsPerSecond
	let numClockTicks: long = long(microSeconds) * clockTicksPerMicrosecond
	return int(numClockTicks / ClockDivider)
}

let rev = toCounterTicks(microSeconds: 1400)
let stop = toCounterTicks(microSeconds: 1500)
let forward = toCounterTicks(microSeconds: 1600)

stop - rev
forward - stop

