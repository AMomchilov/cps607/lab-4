# CPS607 Fall 2017 Lab 4

#### Shane Segal 500591309, Alexander Momchilov 500568636

For this lab, we have altered our bot to perform these tasks, in order of priority:

* Table Edge avoidence (IR based)
* Object (Squid, Wall) avoidence (Ultrasonic sensor based)
  * When an object is detected on the side, the robot adjusts to turn slightly away, until a safe distance is achieved
  * When an object is detected on the side mid-turn (when backing away from a table edge), the robot reverses until it can turn and sucessfully clear the object
  * When an object is detected in fron by any of the 3 front facing sensors, the robot turns to flee.
* Line Following (IR based)
  * Uses an IR sensor array with 5 sensors
  * Line aquisition at *any* angle
  * Corning on *very* sharp turns
  * The robot remembers which of the far sensors was tripped last. When the robot finds itself fallen off a line (such as when attempting a sharp turn, or aquiring a line at 90 degrees), it turns in the direction of that last sensor, until the robot is lined up with the line again.
  * Prioritizes table edge avoidence over line following, so it will never follow a line off a table
* Honing in on a candle (IR based)
  * Uses an array of 5 sensors, and makes adjustments so as to center itself to face the candle
    - The robot waits until the candle is in a good spot (centered, close enough)
  * Prioritizes table edge avoidence over candle chasing, so it will never fall off a table as a result of chasing a candle
* Extinuishing the candle (using a fan)
  * Retries until success:
    * Blows on it with the fan for some time
    * Stops and waits a little (to let the flame rekindle, in case it barely survived)
    * Repeats until the flame fails to rekindle
  * The robot deactivates itself upon success



