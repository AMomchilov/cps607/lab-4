//
//  SuspensionState.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-17.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef SuspensionState_hpp
#define SuspensionState_hpp

#include "TimeUnits.hpp"

class MillisTimerSuspensionState {
private:
	MilliSeconds unsuspendTime;

public:
	MillisTimerSuspensionState();
	MillisTimerSuspensionState(MilliSeconds);
	bool isSuspended();

//	MillisTimerSuspensionState(const MillisTimerSuspensionState&); // Copy Constructor
//	MillisTimerSuspensionState& operator= (const MillisTimerSuspensionState&); // Copy assignment
//	MillisTimerSuspensionState& operator= (MillisTimerSuspensionState&&); /// Move assignment
};

class SuspensionState {
private:
	enum Tag { Ready, IndefinitelySuspended, MSTimerSuspended } tag;
	union {
		MillisTimerSuspensionState millisTimerSuspensionState;

	};

	SuspensionState(Tag);
public:
	static SuspensionState ready();
	static SuspensionState indefinite();
	SuspensionState(MillisTimerSuspensionState);

	bool isSuspended();
};

SuspensionState SuspendFor(MilliSeconds);

#endif /* SuspensionState_hpp */
