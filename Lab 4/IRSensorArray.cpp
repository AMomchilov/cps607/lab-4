//
//  IRSensorArray.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "IRSensorArray.hpp"
#include "CommonImports.hpp"

IRSensorArray::IRSensorArray(
							 uint8_t enablePinNumber,
							 IRSensor leftSensor,
							 IRSensor midLeftSensor,
							 IRSensor midSensor,
							 IRSensor midRightSensor,
							 IRSensor rightSensor
							 ):
enablePin(enablePinNumber),
leftSensor(move(leftSensor)),
midLeftSensor(move(midLeftSensor)),
midSensor(move(midSensor)),
midRightSensor(move(midRightSensor)),
rightSensor(move(rightSensor)),
sensors{
	&this->leftSensor,
	&this->midLeftSensor,
	&this->midSensor,
	&this->midRightSensor,
	&this->rightSensor
}
{}

IRSensorArray::IRSensorArray(const IRSensorArray &&other): //move constructor
enablePin(enablePin),
leftSensor(move(other.leftSensor)),
midLeftSensor(move(other.midLeftSensor)),
midSensor(move(other.midSensor)),
midRightSensor(move(other.midRightSensor)),
rightSensor(move(other.rightSensor)),
sensors{
	&this->leftSensor,
	&this->midLeftSensor,
	&this->midSensor,
	&this->midRightSensor,
	&this->rightSensor
}
{}

void IRSensorArray::enable() {
	enablePin.setLow(); // active low
}

void IRSensorArray::disable() {
	enablePin.setHigh();
}

void IRSensorArray::read() {
	for (auto it = sensors.begin(); it != sensors.end(); ++it) {
		(*it)->read();
	}
}

