//
//  Function.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-25.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef Function_hpp
#define Function_hpp

// An interface for any type with an implemented operator().
template<typename ReturnType, typename... Args>
struct Function {
	virtual ReturnType operator() (Args... args) const = 0;
};

// A type that wraps a function pointer and allows it to be invoked polymorphically.
// Can be initialized by non-capturing lambda expressions (those which are castable to
// a mere function pointer)
template<typename ReturnType, typename... Args>
struct FunctionPtr: public Function<ReturnType, Args...> {
	typedef ReturnType (*RawFunctionPointer)(Args...);
	const RawFunctionPointer rawFnPtr;
	
	FunctionPtr(RawFunctionPointer rawFnPtr): rawFnPtr(rawFnPtr) {}
	
	ReturnType operator() (Args... args) const { return rawFnPtr(args...); }
};


//FIXME: Does this leak the lambdaObject?

// A type that wraps a lambda object and its function pointer, allowing the storage
// of lambda expressions, even those which capture state.
template<typename LambdaType, typename ReturnType, typename... Args>
struct Lambda: public Function<ReturnType, Args...> {
	LambdaType lambdaObject;
	ReturnType (LambdaType::*rawFnPtr)(Args...)const;
	
	Lambda(const LambdaType &lambdaObject):
        lambdaObject(lambdaObject),
        rawFnPtr(&LambdaType::operator())
    {}
    
    ~Lambda() {
        delete lambdaObject;
    }

	
	ReturnType operator() (Args... args) const {
		return (lambdaObject.*rawFnPtr)(args...);
	}
};

#endif /* Function_hpp */
