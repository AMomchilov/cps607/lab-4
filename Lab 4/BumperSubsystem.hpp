//
//  BumperSubsystem.hpp
//  Index
//
//  Created by Shane Segal on 2017-10-26.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef BumperSubsystem_hpp
#define BumperSubsystem_hpp

#include <stdio.h>
#include "CommonImports.hpp"
#include "Bumper.hpp"

class BumperSubsystem
{
private:
	static const int counterClockwisePin = P7_4;
	static const int clockwisePin = P1_3;

public:
	Bumper counterClockwise; // frontLeftBackRight;
	Bumper clockwise;        // frontRightBackLeft;

	bool shouldTurnLeft() const;
	bool shouldTurnRight() const;

	BumperSubsystem();

};

#endif /* BumperSubsystem_hpp */
