//
//  TimeUnits.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "TimeUnits.hpp"

MicroSeconds::MicroSeconds(unsigned long count): count(count) {}

MilliSeconds::MilliSeconds(unsigned long count): count(count) {}
