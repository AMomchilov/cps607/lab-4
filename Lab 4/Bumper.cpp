//
//  Bumper.cpp
//  Index
//
//  Created by Shane Segal on 2017-10-26.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "Bumper.hpp"
#include "CommonImports.hpp"

Bumper::Bumper(int pin):
	pin(pin)
{
    pinMode(pin, INPUT_PULLDOWN);
}

bool Bumper::isHit() const
{
    return digitalRead(pin) == HIGH;
}
