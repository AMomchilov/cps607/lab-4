//
//  DigitalOutput.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-21.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "DigitalOutput.hpp"

DigitalOutput::DigitalOutput(uint8_t pin):
pin(pin) {
	pinMode(this->pin, OUTPUT);
}

// C++0x does not support delegating constructors.
// Repetition makes me sad :(
DigitalOutput::DigitalOutput(uint8_t pin, uint8_t initialState):
pin(pin) {
	pinMode(this->pin, OUTPUT);
	digitalWrite(this->pin, initialState);
}

void DigitalOutput::setHigh() const {
	digitalWrite(this->pin, HIGH);
}

void DigitalOutput::setLow() const {
	digitalWrite(this-> pin, LOW);
}

