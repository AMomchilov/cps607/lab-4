//
//  Bumper.hpp
//  Index
//
//  Created by Shane Segal on 2017-10-26.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef Bumper_hpp
#define Bumper_hpp

#include <stdio.h>

class Bumper
{
public:
  Bumper(int pin);
  bool isHit() const;

private:
    const int pin;
};

#endif /* Bumper_hpp */
