//
//  RunningAverageFilter.h
//  Lab 4
//
//  Created by Alexander Momchilov on 2017-11-13.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef RunningAverageFilter_h
#define RunningAverageFilter_h

#include <deque>
using std::deque;

#include <memory>

template <typename T> class RunningAverageFilter {
public:  //todo: temporarily public
	deque<T> historicalValues;
	float sum;

public:
	RunningAverageFilter(int size, T initialValue):
	historicalValues(/*size, initialValue*/),
	sum(size * initialValue)
	{
		for (int i = 0; i < size; ++i) historicalValues.push_back(initialValue);
	}

	float nextValue(T value) {
		sum -= historicalValues.front();
		historicalValues.pop_front();

		sum += value;
		historicalValues.push_back(value);

		return sum / (float) historicalValues.size();
	}
};

#endif /* RunningAverageFilter_h */
