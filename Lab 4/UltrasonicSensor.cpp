//
//  UltrasonicSensor.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-25.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "UltrasonicSensor.hpp"

#include "CommonImports.hpp"

UltrasonicSensor::UltrasonicSensor(int trigPin, int echoPin):
trigPin(trigPin, LOW),
echoPin(echoPin),
lastReadingCM(-1.0),
filter(5, (float) UltrasonicSensor::longestDetectableDistanceCM)
{
		pinMode(echoPin, INPUT);
}

float UltrasonicSensor::read() {
	trigPin.setLow();
	delayMicroseconds(2);
	trigPin.setHigh(); //10 µs pulse to activate sensor
	delayMicroseconds(10);
	trigPin.setLow();
	
	// Measures how long the HIGH pulse is maintained, which indicates the sound wave travel time, in microseconds
    long pulseDuration = pulseIn(echoPin, HIGH, UltrasonicSensor::timeout);
	float rawDistanceCM = (float) pulseDuration * (float) speedOfSoundCentimetersPerMicroSecond / 2.0;

	// Account for timeout (which causes rawDistanceCM == 0)
	float distanceCM = (rawDistanceCM <= 0.1) ? longestDetectableDistanceCM : rawDistanceCM;

	// Run averaging filter
	float filteredDistanceCM = filter.nextValue(distanceCM);

	// Save last reading for faster access
	this->lastReadingCM = filteredDistanceCM;

	return filteredDistanceCM;
}

float UltrasonicSensor::getLastReading() const {
	return this->lastReadingCM;
}
