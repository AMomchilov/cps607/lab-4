//
//  DigitalOutput.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-21.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef DigitalOutput_hpp
#define DigitalOutput_hpp

#include <stdint.h>
#include "CommonImports.hpp"

class DigitalOutput {
private:
	const uint8_t pin;

public:
	DigitalOutput(uint8_t pin);
	DigitalOutput(uint8_t pin, uint8_t initialState);

	void setHigh() const;
	void setLow() const;
};

#endif /* DigitalOutput_hpp */
