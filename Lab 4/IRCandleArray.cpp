//
//  IRCandleArray.cpp
//  Index
//
//  Created by Shane Segal on 2017-11-19.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "IRCandleArray.hpp"


IRCandleArray::IRCandleArray(
                             int enablePinNumber,
                             IRSensor leftSensor,
                             IRSensor midLeftSensor,
                             IRSensor midSensor,
                             IRSensor midRightSensor,
                             IRSensor rightSensor
                             ):
IRSensorArray(
              enablePinNumber,
              leftSensor,
              midLeftSensor,
              midSensor,
              midRightSensor,
              rightSensor
              )
{
}

CandleDirection IRCandleArray::getCandleDirection(){
    auto max = detectionThreshold;
    CandleDirection dir = CandleDirection::NotFound;
    
    if(leftSensor.getLastReading() > max){
        max = leftSensor.getLastReading();
        dir = CandleDirection::Left;
    }
    if(midLeftSensor.getLastReading() > max){
        max = midLeftSensor.getLastReading();
        dir = CandleDirection::MidLeft;
    }
    if(midSensor.getLastReading() > max){
        max = midSensor.getLastReading();
        dir = CandleDirection::Mid;
    }
    if(midRightSensor.getLastReading() > max){
        max = midRightSensor.getLastReading();
        dir = CandleDirection::MidRight;
    }
    if(rightSensor.getLastReading() > max){
        max = rightSensor.getLastReading();
        dir = CandleDirection::Right;
    }
    
    return dir;
}

bool IRCandleArray::isCandleInFanRange(){
    return (midSensor.getLastReading() >= maxCandleBrightness);
}

