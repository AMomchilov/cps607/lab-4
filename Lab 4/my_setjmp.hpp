//
//  my_setjmp.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-02.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef my_setjmp_hpp
#define my_setjmp_hpp

#include <stdint.h>

struct my_jmp_buf_raw {
    uint32_t r0, r1, r2,
    // skip r3 (it's a read-only constant generator)
    r4, r5, r6, r7, r8, r9, r10, r11;
    // r12, r13, r14, r15 are caller-saved. No need to save/restore them.
};

typedef my_jmp_buf_raw my_jmp_buf[1];

int my_setjmp(my_jmp_buf resumeDestination) asm("my_setjmp");
__attribute__((__noreturn__)) void my_longjmp(my_jmp_buf jumpDestination, int setJmpReturnValue) asm("my_longjmp");

#endif /* my_setjmp_hpp */
