//
//  Scheduler.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-24.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef Scheduler_hpp
#define Scheduler_hpp

#include "CommonImports.hpp"

#include <vector>
using std::vector

#include "Task.hpp"
#include "TimeUnits.hpp"

class Scheduler {
private:
	const vector<Task *> tasks;
//	decltype(tasks.begin()) iterator;

	bool _isActive;
	const MilliSeconds loopPeriod; // The delay between one round of tasks before the next.
	
public:
	
	Scheduler(vector<Task *> tasks, MilliSeconds loopPeriod = MilliSeconds(1000));
	
	bool isActive() const;
	
	// Starts the scheduler, loops until `isActive()` is false.
	void start();
};

#endif /* Scheduler_hpp */
