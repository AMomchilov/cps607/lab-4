//
// Lab 4
//
// ___VARIABLE_projectDescriptionText___
// Developed with [embedXcode](http://embedXcode.weebly.com)
//
// Author         Alexander Momchilov
//                 Alexander Momchilov
//
// Date            2017-10-17 7:45 PM
// Version        <#version#>
//
// Copyright    © Alexander Momchilov, 2017
// Licence        <#licence#>
//
// See         ReadMe.txt for references
//

#include "CommonImports.hpp"

#include <functional>

#include "Scheduler.hpp"
#include "Task.hpp"
#include "SuspensionState.hpp"

#include "IRSensorSubsystem.hpp"
IRSensorSubsystem irSensorSubsystem;

#include "DriveSubsystem.hpp"
DriveSubsystem driveSubsystem;

#include "UltrasonicSensorSubsystem.hpp"
UltrasonicSensorSubsystem ultrasonicSensorSubsystem;

#include "DigitalOutput.hpp"
DigitalOutput fan(P4_3);

char getCharForReading(IRSensorResult sensorReading) {
    switch (sensorReading) {
        case IRSensorResult::Table: return 'T';
        case IRSensorResult::BlackTape: return '#';
        case IRSensorResult::EmptyAir: return 'O';
    }
    
    return '\0'; // Should never happen
}

void printSensorArray(IRSensorArray &sensorArray, bool printFullInts) {
    auto &sensors = sensorArray.sensors;
    for (auto it = sensors.begin(); it != sensors.end(); ++it) {
        auto &sensor = *it;
		auto value = sensor->getLastReading();
//        auto value = sensor->read();
        if (printFullInts) {
            Serial.print(value);
            Serial.print('\t');
        }
        else Serial.print(getCharForReading(sensor->getLastReadingType()));
    }
}

RunFn irSensorReader = new FunctionPtr<void, Task &>(+[](Task &task) {
    while (1) {
        irSensorSubsystem.activeIRSensorArray->read();
        irSensorSubsystem.swapActiveSensorArray();

        bool shouldPrint = 1;
        bool candlePrint = 0;
        bool printFullInts = 1;
        
        if (shouldPrint) {
            printSensorArray(irSensorSubsystem.surfaceIRSensorArray, printFullInts);
            Serial.print("|\t");
            printSensorArray(irSensorSubsystem.candleIRSensorArray, printFullInts);
            Serial.println();
        }
        
        if (candlePrint) {
            Serial.print("Candle is: ");
            auto direction = irSensorSubsystem.candleIRSensorArray.getCandleDirection();
            const char *str = "Not found";
            if(direction == CandleDirection::Left)
                str = "Left";
            else if(direction == CandleDirection::MidLeft)
                str = "MidLeft";
            else if(direction == CandleDirection::Mid)
                str = "Middle";
            else if(direction == CandleDirection::MidRight)
                str = "MidRight";
            else if(direction == CandleDirection::Right)
                str = "Right";
            Serial.println(str);
        }
        
        task.yield(SuspendFor(MilliSeconds(10)));
    }
});

volatile bool isTurningAwayFromTableEdge = false;

void evadeObjectOnSide(Task &parentTask) {
    Serial.print("evadeObjectOnSide()\tleft: ");
    Serial.print(ultrasonicSensorSubsystem.isObjectToTheLeft() ? 'X' : 'O');
    Serial.print("\tright: ");
    Serial.println(ultrasonicSensorSubsystem.isObjectToTheRight() ? 'X' : 'O');
    if (ultrasonicSensorSubsystem.isObjectToTheLeft()) driveSubsystem.turnCW();
    else if (ultrasonicSensorSubsystem.isObjectToTheRight()) driveSubsystem.turnCCW();
    else Serial.println("evadeObjectOnSide() called, but nothing is on the sides???");
    
    parentTask.yield(SuspendFor(MilliSeconds(500)));
}

enum class LastFrontSideUltrasonicSensorTripped { Neither, Left, Right };
LastFrontSideUltrasonicSensorTripped lastFrontSideUltrasonicSensorTripped = LastFrontSideUltrasonicSensorTripped::Neither;

void evadeObjectAhead(Task &parentTask) {
    if (ultrasonicSensorSubsystem.isObjectInFrontLeft())  {
        lastFrontSideUltrasonicSensorTripped = LastFrontSideUltrasonicSensorTripped::Left;
        Serial.println("Object front left");
        driveSubsystem.turnCW();
    }
    else if (ultrasonicSensorSubsystem.isObjectInFrontRight()) {
        lastFrontSideUltrasonicSensorTripped = LastFrontSideUltrasonicSensorTripped::Right;
        Serial.println("Object front right");
        driveSubsystem.turnCCW();
    }
    else if (ultrasonicSensorSubsystem.isObjectInFrontCenter()) {
        Serial.println("Object straight ahead");
        switch (lastFrontSideUltrasonicSensorTripped) {
            case LastFrontSideUltrasonicSensorTripped::Left:
                driveSubsystem.turnCW();
                break;
                
            case LastFrontSideUltrasonicSensorTripped::Right:
                driveSubsystem.turnCCW();
                break;
                
            case LastFrontSideUltrasonicSensorTripped::Neither:
                driveSubsystem.turnRand();
                break;
        }
        lastFrontSideUltrasonicSensorTripped = LastFrontSideUltrasonicSensorTripped::Neither;
    }
    else Serial.println("evadeObjectAhead() called, but nothing is in front???");
    
    parentTask.yield(SuspendFor(MilliSeconds(500)));
}


RunFn edgeBackoffFn = new FunctionPtr<void, Task &>(+[](Task &task) {
    while (1) {
        isTurningAwayFromTableEdge = true;
        switch (irSensorSubsystem.surfaceIRSensorArray.getEdgeStatus()) {
            case IRSurfaceArrayReading::leftOffEdge:
                driveSubsystem.turnCW();
                Serial.println("Left off Edge");
                break;
            case IRSurfaceArrayReading::rightOffEdge:
                driveSubsystem.turnCCW();
                Serial.println("Right off Edge");
                break;
            case IRSurfaceArrayReading::frontOffEdge:
                if (random(0, 2) == 0) driveSubsystem.turnCCW();
                else driveSubsystem.turnCW();
                Serial.println("Front off Edge");
                break;
            case IRSurfaceArrayReading::onTable:
                break;
        }
        task.yield(SuspendFor(MilliSeconds(1500)));
        isTurningAwayFromTableEdge = false;
        task.yield();
    }
});
auto edgeBackoffTask = new Task(edgeBackoffFn, SuspendFor(MilliSeconds(0)), 500);

void resetEdgeBackoffTask() {
//    Serial.println("resetEdgeBackoffTask()");
    edgeBackoffTask->reset();
    isTurningAwayFromTableEdge = false;
}

enum class LastSideSensorTripped { Neither, Left, Right };

// Precond: one of the side sensors was tripped
void evadeObjectOnSideMidTableEdgeTurn(Task &parentTask) {
    UltrasonicSensor *trippedSideSensor = nullptr;
    LastSideSensorTripped lastSideSensorTripped = LastSideSensorTripped::Neither;
    if (ultrasonicSensorSubsystem.isObjectToTheLeft()) {
        trippedSideSensor = &ultrasonicSensorSubsystem.leftSensor;
        lastSideSensorTripped = LastSideSensorTripped::Left;
    }
    else if (ultrasonicSensorSubsystem.isObjectToTheRight()) {
        trippedSideSensor = &ultrasonicSensorSubsystem.rightSensor;
        lastSideSensorTripped = LastSideSensorTripped::Right;
    }
    else Serial.println("I'm lost, how did I get here?");
    
    // Reverse until object has cleared the sensor it once tripped
    //    while (trippedSideSensor->getLastReading() <= UltrasonicSensorSubsystem::SideDetectionThresholdCM) {
    driveSubsystem.reverse(30);
    //        parentTask.yield();
    //    }
    
    // Reverse a bit more
    parentTask.yield(SuspendFor(MilliSeconds(1500)));
    
    switch (lastSideSensorTripped) {
        case LastSideSensorTripped::Left:
            driveSubsystem.turnCCW();
            break;
        case LastSideSensorTripped::Right:
            driveSubsystem.turnCW();
            break;
        case LastSideSensorTripped::Neither:
            Serial.println("Very, very lost.");
            break;
    }
    
    parentTask.yield(SuspendFor(MilliSeconds(2000)));
}

enum class LastFarSensorTripped { Neither, Left, Right };

LastFarSensorTripped lastFarSensorTripped = LastFarSensorTripped::Neither;

RunFn lineFollowingFn = new FunctionPtr<void, Task &>(+[](Task &task) {
    auto BlackTape = IRSensorResult::BlackTape;
    while (1) {
        // Take note of tape on far sides
        if (irSensorSubsystem.surfaceIRSensorArray.leftSensor.getLastReadingType() == BlackTape) {
            lastFarSensorTripped = LastFarSensorTripped::Left;
        }
        else if (irSensorSubsystem.surfaceIRSensorArray.rightSensor.getLastReadingType() == BlackTape) {
            lastFarSensorTripped = LastFarSensorTripped::Right;
        }
        
        // Stay on the tape!
        if (irSensorSubsystem.surfaceIRSensorArray.midLeftSensor.getLastReadingType() == BlackTape) {
            driveSubsystem.drive(10, 50);
        }
        else if (irSensorSubsystem.surfaceIRSensorArray.midRightSensor.getLastReadingType() == BlackTape) {
            driveSubsystem.drive(50, 10);
        }
        else {
            Serial.println("Forward from LineFollowingFn");
            driveSubsystem.forward(30);
        }
        
        // If we fall off the tape, turn towards the last far sensors that was tripped.
        if (!irSensorSubsystem.surfaceIRSensorArray.isOnTape()) {
            do {
                switch (lastFarSensorTripped) {
                    case LastFarSensorTripped::Neither: driveSubsystem.forward(30); break;
                    case LastFarSensorTripped::Left:	driveSubsystem.turnCCW(20);	break;
                    case LastFarSensorTripped::Right:	driveSubsystem.turnCW(20);	break;
                }
                task.yield();
            } while (irSensorSubsystem.surfaceIRSensorArray.midSensor.getLastReadingType() != BlackTape);
            
            lastFarSensorTripped = LastFarSensorTripped::Neither;
        }
        task.yield();
    }
});
auto lineFollowingTask = new Task(lineFollowingFn, SuspendFor(MilliSeconds(0)), 500);

void resetLineFollowingTask() {
//    Serial.println("resetLineFollowingTask()");
    lineFollowingTask->reset();
    lastFarSensorTripped = LastFarSensorTripped::Neither;
}

bool searchingForCandle = false;

void extinguishFlame(Task &parentTask){
    do {
        fan.setHigh(); //turn on --> set high for now, might need to switch
        parentTask.yield(SuspendFor(MilliSeconds(4000)));
        
        fan.setLow(); // turn off
        parentTask.yield(SuspendFor(MilliSeconds(6000))); // wait for 2 seconds to see if flame reignites
	} while (irSensorSubsystem.candleIRSensorArray.getCandleDirection() != CandleDirection::NotFound);
    Serial.println("Exited Candle Extinguish Loop");
}

bool candleFound = false;

void moveTowardsCandle() {
    if(irSensorSubsystem.candleIRSensorArray.isCandleInFanRange()){
        driveSubsystem.stop();
        candleFound = true;
    }else{
        switch(irSensorSubsystem.candleIRSensorArray.getCandleDirection()) {
            case CandleDirection::Left:
                Serial.println("Moving Hard Left towards Candle");
                driveSubsystem.pivotLeft(30);
                break;
            case CandleDirection::MidLeft:
                Serial.println("Moving Left towards Candle");
                driveSubsystem.pivotLeft(15);
                break;
            case CandleDirection::Mid:
                Serial.println("Moving Straight towards Candle");
                driveSubsystem.forward(30);
                break;
            case CandleDirection::MidRight:
                Serial.println("Moving Right towards Candle");
                driveSubsystem.pivotRight(15);
                break;
            case CandleDirection::Right:
                Serial.println("Moving Hard Right towards Candle");
                driveSubsystem.pivotRight(30);
                break;
            default: break;
        }
    }
}



RunFn candleAcquisitionFn = new FunctionPtr<void, Task &>(+[](Task &task) {
    while(1){
        auto didntFindCandle = irSensorSubsystem.candleIRSensorArray.getCandleDirection() == CandleDirection::NotFound;
        if (didntFindCandle) {
            Serial.println("Candle Not Found");
            driveSubsystem.forward();
            task.yield(SuspendFor(MilliSeconds(2000)));
        } else {
            searchingForCandle = true;
            Serial.println("Candle Found");
            moveTowardsCandle();
        }
//        searchingForCandle = false;
        task.yield();
    }
});

auto candleAquisitionTask = new Task(candleAcquisitionFn, SuspendFor(MilliSeconds(0)), 500);

void resetCandleAcquisitionTask(){
    candleAquisitionTask->reset();
    searchingForCandle = false;
    candleFound = false;
}

void resetDriveTasks() {
    resetLineFollowingTask();
    resetEdgeBackoffTask();
    resetCandleAcquisitionTask();
}

void die() {
	driveSubsystem.stop();
	fan.setLow();

	DigitalOutput redLED(P1_0, HIGH);

	while (1);
}

bool evasionModeActive = true;

RunFn mainDriveDispatchingFn = new FunctionPtr<void, Task &>(+[](Task &task) {
    while (1) {
        if (isTurningAwayFromTableEdge && evasionModeActive && ultrasonicSensorSubsystem.isObjectToTheSide()) {
            Serial.println("Interrupted Midturn!");
            evadeObjectOnSideMidTableEdgeTurn(task);
            resetDriveTasks();
        }
        else if (irSensorSubsystem.surfaceIRSensorArray.isOffEdge()) {
            Serial.println("Off edge!");
            if (!edgeBackoffTask->isSuspended()) edgeBackoffTask->run();
            //			Serial.println("Done turning away from edge!");
            
            resetLineFollowingTask();
        }
        else if (!searchingForCandle && evasionModeActive && ultrasonicSensorSubsystem.isObjectInFront()) {
            Serial.println("ultrasonicSensorSubsystem.isObjectIn ***FRONT*** ()");
            evadeObjectAhead(task);
            resetDriveTasks();
        }
        else if (evasionModeActive && ultrasonicSensorSubsystem.isObjectToTheSide()) {
            Serial.println("ultrasonicSensorSubsystem.isObjectToTheSide ***SIDE*** ()");
            evadeObjectOnSide(task);
            resetDriveTasks();
        }
        else { // Not in danger, normal drive routine
//            Serial.println("Else");
            if (isTurningAwayFromTableEdge) {
//                Serial.println("isTurningAwayFromTableEdge");
                if (!edgeBackoffTask->isSuspended()) {
                    edgeBackoffTask->run();
                    resetLineFollowingTask();
                }
            }
            else {
//                Serial.println("Forward");
                if(!candleFound && !candleAquisitionTask->isSuspended()){
//                    Serial.println("Initiating/resuming Aquisition");
                    candleAquisitionTask->run();
                }
                else if(candleFound){
                    extinguishFlame(task);
					resetCandleAcquisitionTask();

					die();
                }
                else if (!lineFollowingTask->isSuspended()){
                    lineFollowingTask->run();
                    resetCandleAcquisitionTask();
                }
                resetEdgeBackoffTask();
            }
        }
        task.yield();
    }
});

RunFn ultrasonicReaderFn = new FunctionPtr<void, Task &>(+[](Task &task) {
    while (1) {
        if (evasionModeActive) ultrasonicSensorSubsystem.readAll();
        
        if (0) {
            Serial.print("L: ");
            Serial.print((int) ultrasonicSensorSubsystem.leftSensor.getLastReading());
            Serial.print("\tFL: ");
            Serial.print((int) ultrasonicSensorSubsystem.frontLeftSensor.getLastReading());
            Serial.print("\tF: ");
            Serial.print((int) ultrasonicSensorSubsystem.frontSensor.getLastReading());
            Serial.print("\tFR: ");
            Serial.print((int) ultrasonicSensorSubsystem.frontRightSensor.getLastReading());
            Serial.print("\tR: ");
            Serial.println((int) ultrasonicSensorSubsystem.rightSensor.getLastReading());
        }
        
        task.yield();
    }
});

RunFn readUserButtonFn = new FunctionPtr<void, Task &>(+[](Task &task) {
    while (1) {
        if (digitalRead(P2_1) == LOW) { // Kill switch
			die();
        }
        else if (digitalRead(P1_1) == LOW) { // Disable Ultrasonic-based evasion
            evasionModeActive = false;
            
            DigitalOutput greenLED(P4_7, HIGH);
        }
        
        task.yield();
    }
});

//#include "UltrasonicSensor.hpp"
//#include <deque>
//using std::deque;
//template <typename T> void printDeque(deque<T> d) {
//	Serial.print("[");
//	for (auto i = d.begin(); i != d.end(); ++i) {
//		Serial.print(*i);
//		Serial.print(", ");
//	}
//	Serial.println("]");
//}

void setup()
{
    Serial.begin(115200);
    Serial.println("#################### Start of setup()");
    
    // User switch pull up resistors:
    pinMode(P1_1, INPUT_PULLUP);
    pinMode(P2_1, INPUT_PULLUP);
    
    driveSubsystem.stop();
    
    //    evasionModeActive = false;
    
    Scheduler scheduler({
        new Task(irSensorReader, SuspendFor(MilliSeconds(0)), 1000),
        new Task(ultrasonicReaderFn, SuspendFor(MilliSeconds(0)), 500),
        new Task(mainDriveDispatchingFn, SuspendFor(MilliSeconds(1000))),
        new Task(readUserButtonFn, SuspendFor(MilliSeconds(0)), 100),
    }, MilliSeconds(0));
    
    scheduler.start();
    Serial.println("End");
}

void loop() {
//	Serial.println("loop");
//	irSensorSubsystem.activeIRSensorArray->read();
//	//        irSensorSubsystem.swapActiveSensorArray();
//
//	bool shouldPrint = 1;
//	bool candlePrint = 0;
//	bool printFullInts = 1;
//
//	if (shouldPrint) {
//		printSensorArray(irSensorSubsystem.surfaceIRSensorArray, printFullInts);
//		Serial.print("|\t");
//		printSensorArray(irSensorSubsystem.candleIRSensorArray, printFullInts);
//		Serial.println();
//	}
//
//	if (candlePrint) {
//		Serial.print("Candle is: ");
//		auto direction = irSensorSubsystem.candleIRSensorArray.getCandleDirection();
//		const char *str = "Not found";
//		if(direction == CandleDirection::Left)
//			str = "Left";
//		else if(direction == CandleDirection::MidLeft)
//			str = "MidLeft";
//		else if(direction == CandleDirection::Mid)
//			str = "Middle";
//		else if(direction == CandleDirection::MidRight)
//			str = "MidRight";
//		else if(direction == CandleDirection::Right)
//			str = "Right";
//		Serial.println(str);
//	}
}

