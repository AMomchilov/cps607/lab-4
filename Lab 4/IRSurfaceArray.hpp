//
//  IRSurfaceArray
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef IRSurfaceArray_hpp
#define IRSurfaceArray_hpp

#include "IRSensor.hpp"
#include "IRSensorArray.hpp"
#include <vector>
using std::vector;

enum class IRSurfaceArrayReading {
    onTable,
    frontOffEdge,
    leftOffEdge,
    rightOffEdge
};

class IRSurfaceArray : public IRSensorArray {
    
public:
    IRSurfaceArray(
                  int enablePinNumber,
                  IRSensor leftSensor,
                  IRSensor midLeftSensor,
                  IRSensor midSensor,
                  IRSensor midRightSensor,
                  IRSensor rightSensor
                  );
    
    bool isOffEdge();
    bool isOnTape();
    IRSurfaceArrayReading getEdgeStatus();
};

#endif /* IRSurfaceArray_hpp */
