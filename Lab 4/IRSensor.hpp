//
//  IRSensor.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-06.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef IRSensor_hpp
#define IRSensor_hpp

enum class IRSensorResult { EmptyAir, BlackTape, Table };

class IRSensorClassifier {
public: //temp3
	// At school:
	static const int EmptyAirUpperLimit = 200;
	static const int BlackTapeUpperLimit = 1200;
};

class IRSensor {
private:
	const int analogInputPin;
	const float multiplier;

	int lastReading;

public:
	IRSensor(int analogInputPin, float multiplier = 1.0);
	int read();

	int getLastReading() const;
	IRSensorResult getLastReadingType();
};

#endif /* IRSensor_hpp */
