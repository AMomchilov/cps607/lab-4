//
//  Task.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-24.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "Task.hpp"

#include "CommonImports.hpp"

static volatile void *mainStackPointer;
static volatile Task *currentTask;

Task::Task(
	RunFn runFn,
	SuspensionState suspensionState,
    size_t stackSize
):
runFn(runFn),
suspensionState(suspensionState),
stackSize(stackSize),
bottomOfStack((char *) malloc(stackSize)),
stackPointer(bottomOfStack + stackSize),
hasYielded(false)
{}

Task::~Task() {
	free(runFn);
    free(this->bottomOfStack);
}

bool Task::isSuspended() {
	return this->suspensionState.isSuspended();
}


void Task::run() {
//     Save to global so it persists after changing the stack pointer
    currentTask = this;
	
    if (SETJMP(((Task *)currentTask)->returnLocation) == 0) {
//		 We got here after just entering this function

        
        if (currentTask->hasYielded) { // We need to resume the fn from previouslyYieldedLocation
            
            LONGJMP(((Task *)currentTask)->previouslyYieldedLocation, 1);
			
        }
		else { // We need to call the fn from the start
            char *stackPointer = currentTask->bottomOfStack + currentTask->stackSize;
            
            asm volatile ("mov r1, %0": "=r" (mainStackPointer));
            
            asm volatile ("mov %0, r1": /* No outputs. */ : "m" (stackPointer)); // Load the Task's stack pointer
            (*currentTask->runFn)(* (Task *)currentTask); // Start the Tasks's main function.
            asm volatile ("mov %0, r1": /* No outputs. */ : "m" (mainStackPointer)); // Restore the mainStackPointer

            ((Task *)currentTask)->hasYielded = false;
        }
    }
    else return;
}

void Task::yield(SuspensionState suspensionState) {
    this->hasYielded = true;
    this->suspensionState = suspensionState;

    if (SETJMP(this->previouslyYieldedLocation) == 0) { // Got here from the call to yield()
        LONGJMP(this->returnLocation, 1);
    } else { // Got here from a long jump to previouslyYieldedLocation
        return;
    }
};

void Task::reset() {
	this->hasYielded = false;
}
