//
//  IRSurfaceArray.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "IRSurfaceArray.hpp"
#include "CommonImports.hpp"

IRSurfaceArray::IRSurfaceArray(
                               int enablePinNumber,
                               IRSensor leftSensor,
                               IRSensor midLeftSensor,
                               IRSensor midSensor,
                               IRSensor midRightSensor,
                               IRSensor rightSensor
                               ):
IRSensorArray(
              enablePinNumber,
              leftSensor,
              midLeftSensor,
              midSensor,
              midRightSensor,
              rightSensor
              )
{
}

bool IRSurfaceArray::isOffEdge() {
    return IRSurfaceArray::getEdgeStatus() != IRSurfaceArrayReading::onTable;
}

bool IRSurfaceArray::isOnTape() {
    for (auto it = sensors.begin(); it != sensors.end(); ++it) {
        if ((*it)->getLastReadingType() == IRSensorResult::BlackTape) return true;
    }
    return false;
}

IRSurfaceArrayReading IRSurfaceArray::getEdgeStatus() {
    auto air = IRSensorResult::EmptyAir;
	
    auto leftIsOffEdge = this->leftSensor.getLastReadingType() == air;
    auto rightIsOffEdge = this->rightSensor.getLastReadingType() == air;
    
    if (leftIsOffEdge && rightIsOffEdge) return IRSurfaceArrayReading::frontOffEdge;
    if (leftIsOffEdge) return IRSurfaceArrayReading::leftOffEdge;
    if (rightIsOffEdge) return IRSurfaceArrayReading::rightOffEdge;
    return IRSurfaceArrayReading::onTable;
}
