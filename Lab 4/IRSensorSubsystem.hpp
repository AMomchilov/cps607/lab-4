//
//  IRSensorSubsystem.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef IRSensorSubsystem_hpp
#define IRSensorSubsystem_hpp

#include "IRSensorArray.hpp"
#include "IRSurfaceArray.hpp"
#include "IRCandleArray.hpp"

class IRSensorSubsystem {
public:
	IRSurfaceArray surfaceIRSensorArray;
	IRCandleArray candleIRSensorArray;
	IRSensorArray *activeIRSensorArray;

	IRSensorSubsystem();

	void swapActiveSensorArray();

private: //Do not implement these:
	IRSensorSubsystem(const IRSensorSubsystem&); // Copy Constructor
	IRSensorSubsystem& operator= (const IRSensorSubsystem& other); // Copy assignment
};

#endif /* IRSensorSubsystem_hpp */
