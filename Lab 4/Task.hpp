//
//  Task.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-24.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef Task_hpp
#define Task_hpp

#include <cstdlib>
;

#include "my_setjmp.hpp"
#include "Function.hpp"
#include "SuspensionState.hpp"

#define JMP_BUF my_jmp_buf
#define LONGJMP my_longjmp
#define SETJMP my_setjmp

class Task;
typedef Function<void, Task &>* RunFn;

class Task {
private:
public:
	// The function that implements the purpose of this task.
    const RunFn runFn;

    SuspensionState suspensionState;
	
	// TODO: change these to use vector<char>:
    const size_t stackSize; // The size of this Tasks's unqiue runtime stack.
    char *bottomOfStack; // The start address of this Task's unique runtime stack.
    volatile char *stackPointer; // The stack pointer last used by the task, initially pointing to the top.
    JMP_BUF returnLocation; // The location to return to after a yield
    JMP_BUF previouslyYieldedLocation; // The previously yielded location, if any.
	volatile bool hasYielded; // true: fn last yielded, false: fn hasn't run, or last ran until full completion (return)
    
    
public:
	Task(RunFn, SuspensionState = SuspensionState::ready(), size_t stackSize = 1024);
	~Task();
	
	bool isSuspended();
	void run();
	void yield(SuspensionState = SuspensionState::ready());
	void reset();

	//Do not implement these:
	Task(const Task&); // Copy Constructor
	Task& operator= (const Task& other); // Copy assignment
	Task& operator= (Task&& other); /// Move assignment


};

#endif /* Task_hpp */
