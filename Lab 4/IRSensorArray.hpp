//
//  IRSensorArray.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef IRSensorArray_hpp
#define IRSensorArray_hpp

#include <stdint.h>

#include <vector>
using std::vector;

#include "IRSensor.hpp"
#include "DigitalOutput.hpp"

class IRSensorArray {
private:
	const DigitalOutput enablePin;

public:
	IRSensor leftSensor;
	IRSensor midLeftSensor;
	IRSensor midSensor;
	IRSensor midRightSensor;
	IRSensor rightSensor;
	const vector<IRSensor *> sensors;

	IRSensorArray(
				  uint8_t enablePinNumber,
				  IRSensor leftSensor,
				  IRSensor midLeftSensor,
				  IRSensor midSensor,
				  IRSensor midRightSensor,
				  IRSensor rightSensor
				  );

	void enable();
	void disable();

	void read();

	IRSensorArray(const IRSensorArray&&); //move constructor

private: //Do not implement these:
	IRSensorArray(const IRSensorArray&); // Copy Constructor
	IRSensorArray& operator= (const IRSensorArray& other); // Copy assignment
};


#endif /* IRSensorArray_hpp */
