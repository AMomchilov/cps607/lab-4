//
//  TimeUnits.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef TimeUnits_hpp
#define TimeUnits_hpp

class MicroSeconds {
private:
	unsigned long count;
public:
	MicroSeconds(unsigned long);
	operator unsigned long() const { return count; }
};

class MilliSeconds {
private:
	unsigned long count;
public:
	MilliSeconds(unsigned long);
	operator unsigned long() const { return count; }
};

#endif /* TimeUnits_hpp */
