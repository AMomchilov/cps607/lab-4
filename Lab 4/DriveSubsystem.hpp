//
//  DriveSubsystem.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef DriveSubsystem_hpp
#define DriveSubsystem_hpp

#include "CommonImports.hpp"
#include <stdint.h>
#include "HardwarePWMServo.hpp"

class DriveSubsystem
{
private:
	void rawDrive(int left, int right);
public:
	static const int defaultSpeed = 100;

	HardwarePWMServo leftServo;
	HardwarePWMServo rightServo;

	DriveSubsystem();

	void drive(int left, int right);

	void stop();
	void forward(int speed = defaultSpeed);
	void reverse(int speed = defaultSpeed);

	void pivotLeft(int speed = defaultSpeed);
	void pivotRight(int speed = defaultSpeed);

	void turnCW(int speed = defaultSpeed / 2);
	void turnCCW(int speed = defaultSpeed / 2);
	void turnRand(int speed = defaultSpeed / 2);
};

#endif /* DriveSubsystem_hpp */
