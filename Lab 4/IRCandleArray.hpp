//
//  IRCandleArray.hpp
//  Index
//
//  Created by Shane Segal on 2017-11-19.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef IRCandleArray_hpp
#define IRCandleArray_hpp

#include <stdio.h>
#include "IRSensorArray.hpp"

enum class CandleDirection {Left, MidLeft, Mid, MidRight, Right, NotFound};

class IRCandleArray : public IRSensorArray{
public:
    IRCandleArray(
                  int enablePinNumber,
                  IRSensor leftSensor,
                  IRSensor midLeftSensor,
                  IRSensor midSensor,
                  IRSensor midRightSensor,
                  IRSensor rightSensor
                  );
    CandleDirection getCandleDirection();
    bool isCandleInFanRange();
    
private:
    static const int detectionThreshold = 200;
    static const int maxCandleBrightness = 3400;
};

#endif /* IRCandleArray_hpp */
