//
//  IRSensorSubsystem.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "CommonImports.hpp"

#include "IRSensorSubsystem.hpp"

IRSensorSubsystem::IRSensorSubsystem():
surfaceIRSensorArray(
					 P2_0, //frontIRSensorEnablePin
					 IRSensor(A1, 0.7534f), //0.689f),
					 IRSensor(A2, 1.0899f), //1.040f),
					 IRSensor(A3, 1.4346f), //1.446f),
					 IRSensor(A4, 0.8038f), //0.825f),
					 IRSensor(A12, 1.2285f) //1.459f)
					 ),
candleIRSensorArray(
					P2_4, //backIRSensorEnablePin
					IRSensor(A12),
					IRSensor(A4),
					IRSensor(A3),
					IRSensor(A2),
					IRSensor(A1)
					),
activeIRSensorArray(&(this->surfaceIRSensorArray))
{
	surfaceIRSensorArray.enable();
	candleIRSensorArray.disable();
}

void IRSensorSubsystem::swapActiveSensorArray()
{
	activeIRSensorArray->disable(); //disable old

	if (activeIRSensorArray == &surfaceIRSensorArray) activeIRSensorArray = &candleIRSensorArray;
	else activeIRSensorArray = &surfaceIRSensorArray;

	activeIRSensorArray->enable(); //enable new
}
