//
//  HardwarePWMServo.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-01.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//
#ifndef HardwarePWMServo_hpp
#define HardwarePWMServo_hpp

#include "stdint.h"
#include "driverlib.h"

class HardwarePWMServo {
public: // FIXME: temp
	static const uint32_t ProcessorClock 	= 16000000ul; // 15990784ul Hz rounded
	static const uint32_t ClockDivider		= 8;
	static const uint32_t PWMFrequency		= 100; // in Hz
	static const uint16_t PWMPeriod =
		ProcessorClock / (PWMFrequency * ClockDivider);	// PWM Period in counter ticks

	static uint16_t toCounterTicks(uint16_t microSeconds);

	const uint32_t timerBaseAddress;
	const uint16_t compareRegister;

public:
	HardwarePWMServo(uint16_t GPIOPort,
					 uint16_t GPIOPin,
					 uint32_t timerBaseAddress,
					 uint16_t clockSource,
					 uint16_t clockSourceDivider,
					 uint16_t compareRegister,
					 uint16_t dutyCycle);

	//TODO: use Microseconds type
	void setUptime(uint16_t microseconds);
};

#endif /* HardwarePWMServo_hpp */
