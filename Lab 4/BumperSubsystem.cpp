//
//  BumperSubsystem.cpp
//  Index
//
//  Created by Shane Segal on 2017-10-26.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "BumperSubsystem.hpp"

BumperSubsystem::BumperSubsystem():
counterClockwise(counterClockwisePin),
clockwise(clockwisePin)
{
}

bool BumperSubsystem::shouldTurnRight() const
{
    return counterClockwise.isHit();
}

bool BumperSubsystem::shouldTurnLeft() const
{
    return clockwise.isHit();
}
