//
//  UltrasonicSensorSubsystem.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-26.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef UltrasonicSensorSubsystem_hpp
#define UltrasonicSensorSubsystem_hpp

#include "CommonImports.hpp"

#include "UltrasonicSensor.hpp"

class UltrasonicSensorSubsystem {
private:
	static const int RightSensorTrigPin			= P8_2;
	static const int RightSensorEchoPin			= P2_3;
	static const int FrontRightSensorTrigPin	= P3_7;
	static const int FrontRightSensorEchoPin	= P2_6;
	static const int FrontSensorTrigPin		 	= P4_0;
	static const int FrontSensorEchoPin		 	= P3_1;
	// Unused pin here
	static const int FrontLeftSensorTrigPin		= P3_0;
	static const int FrontLeftSensorEchoPin 	= P1_2;
	// Reset pin here
	static const int LeftSensorTrigPin			= P1_3;
	static const int LeftSensorEchoPin			= P7_4;

public:
	static constexpr float FrontDetectionThresholdCM = 16;
	static constexpr float SideDetectionThresholdCM = 16;
    static constexpr float CandleDetectionDistance = 5;
	
public:
	UltrasonicSensor rightSensor;
	UltrasonicSensor frontRightSensor;
	UltrasonicSensor frontSensor;
	UltrasonicSensor frontLeftSensor;
	UltrasonicSensor leftSensor;
	
	UltrasonicSensorSubsystem();
	
	void readAll();
	
	bool isObjectToTheRight();
	bool isObjectToTheLeft();
	bool isObjectToTheSide();

	bool isObjectInFrontLeft();
	bool isObjectInFrontCenter();
	bool isObjectInFrontRight();
	bool isObjectInFront();
    
    bool isCandleInFront();
};

#endif /* UltrasonicSensorSubsystem_hpp */
