//
//  SuspensionState.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-17.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "SuspensionState.hpp"

#include "CommonImports.hpp"

MillisTimerSuspensionState::MillisTimerSuspensionState():
unsuspendTime(0)
{}

MillisTimerSuspensionState::MillisTimerSuspensionState(MilliSeconds suspensionDuration):
unsuspendTime(millis() + suspensionDuration)
{}

bool MillisTimerSuspensionState::isSuspended() {
	return millis() < unsuspendTime;
}


//// Copy Constructor
//MillisTimerSuspensionState::MillisTimerSuspensionState(const MillisTimerSuspensionState& source):
//unsuspendTime(source.unsuspendTime)
//{}
//
//// Copy assignment
//MillisTimerSuspensionState& MillisTimerSuspensionState::operator= (const MillisTimerSuspensionState& source) {
//	this->unsuspendTime = source.unsuspendTime;
//	return *this;
//};

// Move assignment
//MillisTimerSuspensionState& MillisTimerSuspensionState::operator= (MillisTimerSuspensionState&& other) {
//}

bool SuspensionState::isSuspended() {
	switch (tag) {
		case SuspensionState::Ready: return false;
		case SuspensionState::IndefinitelySuspended: return true;
		case SuspensionState::MSTimerSuspended: return this->millisTimerSuspensionState.isSuspended();
	}

	return false;
}

SuspensionState::SuspensionState(MillisTimerSuspensionState msss):
tag(SuspensionState::MSTimerSuspended),
millisTimerSuspensionState(msss)
{}

SuspensionState::SuspensionState(Tag tag):
tag(tag)
{}

SuspensionState SuspensionState::ready() {
	return SuspensionState(SuspensionState::Ready);
}

SuspensionState SuspensionState::indefinite() {
	return SuspensionState(SuspensionState::IndefinitelySuspended);
}

SuspensionState SuspendFor(MilliSeconds ms) {
	return SuspensionState(MillisTimerSuspensionState(ms));
}

