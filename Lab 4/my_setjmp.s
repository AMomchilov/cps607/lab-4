.section .text.sjlj

.global    my_setjmp
.type    my_setjmp, @function
.func    my_setjmp
my_setjmp:
; Original implementation
; pop    20(r15)
; mov    r1,    0(r15)
; mov    r2,    2(r15)
; mov    r4,    4(r15)
; mov    r5,    6(r15)
; mov    r6,    8(r15)
; mov    r7,    10(r15)
; mov    r8,    12(r15)
; mov    r9,    14(r15)
; mov    r10,    16(r15)
; mov    r11,    18(r15)
; clr    r15
; br    -2(r1)


; modified implementation
; FIXME popx.w relies on this funciton being called with call, not calla
popx.w    r14           ; pop return address into r14
mova    r14, 40(r15)    ; stash program counter
mova    r1 ,  0(r15)    ; stash the stack pointer
mova    r2 ,  4(r15)    ; stash the status register
; skip r3 (it's a read-only constant generator)
mova    r4 ,  8(r15)
mova    r5 , 12(r15)
mova    r6 , 16(r15)
mova    r7 , 20(r15)
mova    r8 , 24(r15)
mova    r9 , 28(r15)
mova    r10, 32(r15)
mova    r11, 36(r15)
; r12, r13, r14, r15 are caller-saved. No need to save/restore them.
clr     r15
bra     r14


.endfunc



.global my_longjmp
.type    my_longjmp, @function
.func    my_longjmp
my_longjmp:

;Original implementation
;mov    r15,    r13
;mov    r14,    r15
;mov    @r13+,    r1
;mov    @r13+,    r2
;mov    @r13+,    r4
;mov    @r13+,    r5
;mov    @r13+,    r6
;mov    @r13+,    r7
;mov    @r13+,    r8
;mov    @r13+,    r9
;mov    @r13+,    r10
;mov    @r13+,    r11
;br    @r13

; Modified implementation
mova     r15 , r13    ; Preserve all available bits of jumpDestination pointer
mov      r14 , r15    ; Sets the int return value of the corresponding setjmp
mova    @r13+, r1
mova    @r13+, r2
; skip r3 (it's a read-only constant generator)
mova    @r13+, r4
mova    @r13+, r5
mova    @r13+, r6
mova    @r13+, r7
mova    @r13+, r8
mova    @r13+, r9
mova    @r13+, r10
mova    @r13+, r11
; r12, r13, r14, r15 are caller-saved. No need to save/restore them.
bra     @r13
.endfunc

