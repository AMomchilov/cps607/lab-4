//
//  UltrasonicSensorSubsystem.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-26.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "UltrasonicSensorSubsystem.hpp"

UltrasonicSensorSubsystem::UltrasonicSensorSubsystem():
rightSensor(RightSensorTrigPin, RightSensorEchoPin),
frontRightSensor(FrontRightSensorTrigPin, FrontRightSensorEchoPin),
frontSensor(FrontSensorTrigPin, FrontSensorEchoPin),
frontLeftSensor(FrontLeftSensorTrigPin, FrontLeftSensorEchoPin),
leftSensor(LeftSensorTrigPin, LeftSensorEchoPin)
{}

void UltrasonicSensorSubsystem::readAll() {
	// Ordered funny in an attempt to reduce cross-talk
	frontSensor.read();
	leftSensor.read();
	frontRightSensor.read();
	rightSensor.read();
	frontLeftSensor.read();
}



bool UltrasonicSensorSubsystem::isObjectToTheLeft() {
	return leftSensor.getLastReading() <= UltrasonicSensorSubsystem::SideDetectionThresholdCM;
}

bool UltrasonicSensorSubsystem::isObjectToTheRight() {
	return rightSensor.getLastReading() <= UltrasonicSensorSubsystem::SideDetectionThresholdCM;
}

bool UltrasonicSensorSubsystem::isObjectToTheSide() {
	return isObjectToTheLeft() || isObjectToTheRight();
}

bool UltrasonicSensorSubsystem::isObjectInFrontLeft() {
	return frontLeftSensor.getLastReading() <= UltrasonicSensorSubsystem::FrontDetectionThresholdCM;
}

bool UltrasonicSensorSubsystem::isObjectInFrontCenter() {
	return frontSensor.getLastReading() <= UltrasonicSensorSubsystem::FrontDetectionThresholdCM;
}

bool UltrasonicSensorSubsystem::isObjectInFrontRight() {
	return frontRightSensor.getLastReading() <= UltrasonicSensorSubsystem::FrontDetectionThresholdCM;
}

bool UltrasonicSensorSubsystem::isObjectInFront() {
	return isObjectInFrontLeft() || isObjectInFrontCenter() || isObjectInFrontRight();
}

bool UltrasonicSensorSubsystem::isCandleInFront(){
    return frontSensor.getLastReading() <= UltrasonicSensorSubsystem::CandleDetectionDistance;
}

