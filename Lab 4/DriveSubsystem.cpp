//
//  DriveSubsystem.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-09-27.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "DriveSubsystem.hpp"

DriveSubsystem::DriveSubsystem() : leftServo(
                                       GPIO_PORT_P1,
                                       GPIO_PIN5,
                                       TIMER_A0_BASE,
                                       TIMER_A_CLOCKSOURCE_SMCLK,
                                       TIMER_A_CLOCKSOURCE_DIVIDER_8,
                                       TIMER_A_CAPTURECOMPARE_REGISTER_4,
                                       1500),
                                   rightServo(
                                       GPIO_PORT_P1,
                                       GPIO_PIN4,
                                       TIMER_A0_BASE,
                                       TIMER_A_CLOCKSOURCE_SMCLK,
                                       TIMER_A_CLOCKSOURCE_DIVIDER_8,
                                       TIMER_A_CAPTURECOMPARE_REGISTER_3,
                                       1500)
{
}

void DriveSubsystem::drive(int left, int right)
{
	//cw is 1500 - n for left wheel
	//cw is 1500 + n for right wheel
	int leftSpeed = 1500 - left;
	int rightSpeed = 1500 + right;

	leftServo.setUptime(leftSpeed);
	rightServo.setUptime(rightSpeed);
}

void DriveSubsystem::stop() { drive(0, 0); }
void DriveSubsystem::forward(int speed) { drive(speed, speed); }
void DriveSubsystem::reverse(int speed) { drive(-speed, -speed); }

void DriveSubsystem::pivotLeft(int speed) { drive(0, speed); }
void DriveSubsystem::pivotRight(int speed) { drive(speed, 0); }

void DriveSubsystem::turnCCW(int speed) { drive(-speed, speed); }
void DriveSubsystem::turnCW(int speed) { drive(speed, -speed); }
void DriveSubsystem::turnRand(int speed) {
	if (random(0, 2) == 0) turnCCW(speed);
    else turnCW(speed);
}

//void DriveSubsystem::turnAround(DetectionDirection detection)
//{
//	IRSensorSubsystem ir;
//
//	ir.swapActiveSensorArray();
//
//	//rotate until the back sensors detect the edge.
//	//dont wanna check the particular sensor as this variance will give us
//	//varying angles of reflection off the edge of the table
//	while (!ir.isOffEdge())
//	{
//		if (detection == Left)
//			drive(speed, -speed); //detection is to the left so we wanna rotate away from it
//		else
//			drive(-speed, speed); //detection is to the right
//	}
//	ir.swapActiveSensorArray(); //move back to the front sensors.
//}
