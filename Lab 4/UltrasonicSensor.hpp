//
//  UltrasonicSensor.hpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-25.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef UltrasonicSensor_hpp
#define UltrasonicSensor_hpp

#include "DigitalOutput.hpp"

#include "RunningAverageFilter.hpp"

class UltrasonicSensor {
public:
	static constexpr float speedOfSoundCentimetersPerMicroSecond = 0.03429;
    static const int longestDetectableDistanceCM = 100;

	// Timout after time it takes sounds go 600cm meters (3 meters out and back)
	// doubled just to be sure
    static const unsigned long timeout = (2 * longestDetectableDistanceCM) / speedOfSoundCentimetersPerMicroSecond;
//	static const unsigned long timeout = 1000000;
private:
	const DigitalOutput trigPin;
	const int echoPin;
	float lastReadingCM;
	RunningAverageFilter<float> filter;
	
public:
	UltrasonicSensor(int trigPin, int echoPin);
	float read();
	float getLastReading() const;
};

#endif /* UltrasonicSensor_hpp */
