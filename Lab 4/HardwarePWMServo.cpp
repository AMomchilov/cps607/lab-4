//
//  HardwarePWMServo.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-01.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "HardwarePWMServo.hpp"

#include "inc/hw_regaccess.h"

uint16_t HardwarePWMServo::toCounterTicks(uint16_t microSeconds) {
	const uint32_t microsecondsPerSecond = 1000000l;
	const uint32_t processorTicksPerMicrosecond = HardwarePWMServo::ProcessorClock / microsecondsPerSecond;
	const uint32_t numProcessorTicks = ((uint32_t) microSeconds) * processorTicksPerMicrosecond;
	return numProcessorTicks / HardwarePWMServo::ClockDivider;
}

//TODO: Parameterize frequency
HardwarePWMServo::HardwarePWMServo(uint16_t GPIOPort,
								   uint16_t GPIOPin,
								   uint32_t timerBaseAddress,
								   uint16_t clockSource,
								   uint16_t clockSourceDivider,
								   uint16_t compareRegister,
								   uint16_t stoppedUptime):
	timerBaseAddress(timerBaseAddress),
	compareRegister(compareRegister) {
		TIMER_A_generatePWM(timerBaseAddress,
							clockSource,
							clockSourceDivider,
							HardwarePWMServo::PWMPeriod,
							compareRegister,
							TIMER_A_OUTPUTMODE_RESET_SET,
							toCounterTicks(stoppedUptime));

		GPIO_setAsPeripheralModuleFunctionOutputPin(GPIOPort, GPIOPin);
}

void HardwarePWMServo::setUptime(uint16_t microseconds) {
	HWREG16(this->timerBaseAddress +
			this->compareRegister +
			OFS_TAxR) = toCounterTicks(microseconds);
}
