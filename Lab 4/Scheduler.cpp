//
//  Scheduler.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-10-24.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//
#include "Scheduler.hpp"

Scheduler::Scheduler(vector<Task *> tasks, MilliSeconds loopPeriod):
tasks(tasks),
//iterator(tasks.begin()),
_isActive(true),
loopPeriod(loopPeriod)
	{}

bool Scheduler::isActive() const {
	return _isActive;
}

void Scheduler::start() {
    while (this->isActive()) {
//        Serial.print("\n\n============================= loop ");
//        Serial.println(millis());

		for (auto iterator = tasks.begin(); iterator != tasks.end(); ++iterator) {
			auto &task = **iterator;
			if (!task.isSuspended()) task.run();
		}
		
        delay(this->loopPeriod);
	}
}

//void Scheduler::runNext() {
//	auto &task = **iterator;
//	if (!task.isSuspended()) task.run();
//
//	if (++iterator == tasks.end()) iterator = tasks.begin();
//}

