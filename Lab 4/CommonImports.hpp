//
//  CommonImports.hpp
//  Lab 4
//
//  Created by Alexander Momchilov on 2017-10-24.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#ifndef CommonImports_h
#define CommonImports_h

// Core library for code-sense - IDE-based
#if defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.8 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

template <class T>
struct remove_reference { typedef T type; };

template <class T>
struct remove_reference<T&> { typedef T type; };

template <class T>
struct remove_reference<T&&> { typedef T type; };

template<class T>
typename remove_reference<T>::type&&
move( T&& arg ) noexcept
{
	return static_cast<typename remove_reference<T>::type&&>( arg );
}

#endif /* CommonImports_h */
