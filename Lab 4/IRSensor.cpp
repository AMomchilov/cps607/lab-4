//
//  IRSensor.cpp
//  Index
//
//  Created by Alexander Momchilov on 2017-11-06.
//  Copyright © 2017 Alexander Momchilov. All rights reserved.
//

#include "IRSensor.hpp"
#include "CommonImports.hpp"

IRSensor::IRSensor(int analogInputPin, float multiplier):
analogInputPin(analogInputPin),
multiplier(multiplier),
lastReading(0)
{
	pinMode(analogInputPin, INPUT);
}

int IRSensor::read() {
	int reading = analogRead(this->analogInputPin);
	this->lastReading = (multiplier == 1.0f) ? reading : (int) ((float) reading * multiplier);
	return this->lastReading;
}

int IRSensor::getLastReading() const {
	return this->lastReading;
}

IRSensorResult IRSensor::getLastReadingType() {
	auto reading = this->lastReading;
	if (reading < IRSensorClassifier::EmptyAirUpperLimit) return IRSensorResult::EmptyAir; // [0, EmptyAirUpperLimit)
	else if (reading < IRSensorClassifier::BlackTapeUpperLimit) return IRSensorResult::BlackTape; // [EmptyAirUpperLimit, BlackTapeUpperLimit)
	else return IRSensorResult::Table;  // [EmptyAirUpperLimit, inf)
}
